const BASE_10 = 10;

const priv = Symbol('private properties');

const NOOP = Function.prototype;

function autoReport(contextInstance) {
  process.stderr.write(
    `PROFILER[${contextInstance.name}]: ${JSON.stringify(
      contextInstance.stats,
      null,
      2
    )}\n`
  );
  const timeout = setTimeout(
    () => autoReport(contextInstance),
    contextInstance[priv].autoReportingIntervalMS
  );
  timeout.unref();
}

class ProfilerCommand {
  constructor(name) {
    this[priv] = {
      name,
      count: 0,
      duration: { secs: 0, nanoSecs: 0 },
      started: 0
    };
  }

  get started() {
    return this[priv].started;
  }

  get stats() {
    const { count, duration } = this[priv];
    const { secs, nanoSecs } = duration;
    const average = count ? (secs * 1e9 + nanoSecs) / count : NaN;
    const durationS = `${secs}.${nanoSecs.toString(BASE_10).padStart(9, '0')}`;
    return {
      count,
      duration,
      durationS,
      averageNS: average
    };
  }

  enter() {
    const privs = this[priv];
    this[priv].count += 1;
    const startHRTime = process.hrtime();
    privs.started += 1;
    let exited = false;
    return () => {
      if (exited) {
        throw new Error(`Command '${privs.name}' was already exited`);
      }
      exited = true;
      const [secs, nanoSecs] = process.hrtime(startHRTime);
      const { duration } = privs;
      privs.started -= 1;
      duration.secs += secs;
      duration.nanoSecs += nanoSecs;
      /* istanbul ignore if */
      if (duration.nanoSecs >= 1e9) {
        duration.nanoSecs -= 1e9;
        duration.secs += 1;
      }
    };
  }

  reset() {
    const privs = this[priv];

    privs.count = 0;
    privs.duration.secs = 0;
    privs.duration.nanoSecs = 0;
  }
}

class Profiler {
  constructor(name, autoReportingIntervalMS = 0) {
    this[priv] = {
      autoReportingIntervalMS,
      name,
      commands: {}
    };
    if (autoReportingIntervalMS) {
      const timeout = setTimeout(
        () => autoReport(this),
        autoReportingIntervalMS
      );
      timeout.unref();
    }
  }

  command(commandName) {
    return this[priv].commands[commandName] || undefined;
  }

  get name() {
    return this[priv].name;
  }

  get stats() {
    const returnValue = {};
    // Object.entries(this[priv].commands).forEach(([commandName, command]) => {
    //   returnValue[commandName] = command.stats;
    // });
    Object.entries(this[priv].commands)
      .map(([name, { stats }]) => ({ name, stats }))
      // sort by duration - longest running first
      .sort(
        /* istanbul ignore next */
        ({ stats: { duration: a } }, { stats: { duration: b } }) =>
          b.secs === a.secs ? b.nanoSecs - a.nanoSecs : b.secs - a.secs
      )
      .forEach(c => {
        const {
          stats: { duration: ignored, ...stats }
        } = c;
        returnValue[c.name] = stats;
      });
    return returnValue;
  }

  createCommand(commandName) {
    const privs = this[priv];
    if (!privs.commands[commandName]) {
      privs.commands[commandName] = new ProfilerCommand(commandName);
    }
    return privs.commands[commandName];
  }

  enter(commandName = '') {
    const privs = this[priv];
    if (!privs.commands[commandName]) {
      privs.commands[commandName] = new ProfilerCommand(commandName);
    }
    const command = privs.commands[commandName];
    return command.enter();
  }

  reset(commandName = '') {
    const command = this[priv].commands[commandName];
    if (!command) {
      return false;
    }
    command.reset();
    return true;
  }
}

/* istanbul ignore next */
const profiling =
  process.env.OLVIN_PROFILER && parseInt(process.env.OLVIN_PROFILER, BASE_10);

const noopInstance = {
  command: NOOP,
  name: 'noop',
  stats: {},
  createCommand: NOOP,
  enter: () => NOOP,
  reset: NOOP
};
let toExport;
/* istanbul ignore if */
if (profiling) {
  toExport = new Profiler('profiler', profiling);
} else {
  toExport = noopInstance;
}
toExport.Profiler = Profiler;
toExport.profiling = profiling;
toExport.noop = noopInstance;

module.exports = toExport;
