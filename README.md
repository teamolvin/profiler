# Profiler

Manually time the execution of blocks of code.

Set `OLVIN_PROFILER` environment variable to a positive number to have the standard profiler report to stderr automatically.
