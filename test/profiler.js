/* eslint-disable no-unused-expressions */

const { expect } = require('chai');
const sinon = require('sinon');

const profilerMod = require('../');

describe('Profiler', () => {
  describe('Profiles', () => {
    it('Creates a new instance', () => {
      const INSTANCE_NAME = 'aap';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      expect(profiler.name).to.equal(INSTANCE_NAME);
    });

    it('Times execution of a command, and the amount of times it was executed', () => {
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      let pRun;

      pRun = profiler.enter(COMMAND_NAME);
      pRun();
      pRun = profiler.enter(COMMAND_NAME);
      pRun();

      expect(profiler.stats[COMMAND_NAME].count).to.equal(2);
      expect(profiler.stats[COMMAND_NAME].durationS).to.be.a('string');
      expect(profiler.stats[COMMAND_NAME].averageNS).to.be.a('number');
    });

    it('Noop instance behaves identical on normal execution', () => {
      const COMMAND_NAME = 'noot';
      const profiler = profilerMod.noop;
      const pRun = profiler.enter(COMMAND_NAME);
      pRun();
    });

    it('Cannot exit a command more than once', () => {
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      const pRun = profiler.enter(COMMAND_NAME);
      pRun();
      expect(pRun).to.throw();
    });

    it('Command name is optional', () => {
      const INSTANCE_NAME = 'aap';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      let pRun;

      pRun = profiler.enter();
      pRun();
      pRun = profiler.enter();
      pRun();

      expect(profiler.stats[''].count).to.equal(2);
      expect(profiler.stats[''].durationS).to.be.a('string');
      expect(profiler.stats[''].averageNS).to.be.a('number');
    });

    it('Can create a command without executing it', () => {
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      const command = profiler.createCommand(COMMAND_NAME);

      expect(profiler.createCommand(COMMAND_NAME)).to.equal(command);
      expect(command.stats.count).to.equal(0);
      expect(profiler.stats[COMMAND_NAME].count).to.equal(0);
      expect(profiler.stats[COMMAND_NAME].durationS).to.equal('0.000000000');
      expect(profiler.stats[COMMAND_NAME].averageNS).to.be.NaN;
    });

    it('Can reset a command', () => {
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      const command = profiler.createCommand(COMMAND_NAME);

      expect(command.stats.count).to.equal(0);

      const pRun = profiler.enter(COMMAND_NAME);
      expect(command.started).to.equal(1);
      pRun();

      expect(command.stats.count).to.equal(1);
      expect(command.stats.duration.nanoSecs).to.be.above(0);
      expect(profiler.stats[COMMAND_NAME].count).to.equal(1);
      expect(profiler.command('MISSING')).to.be.undefined;
      expect(profiler.command(COMMAND_NAME).stats.count).to.equal(1);

      command.reset();

      expect(command.stats.count).to.equal(0);
      expect(profiler.stats[COMMAND_NAME].count).to.equal(0);
      expect(command.stats.duration.nanoSecs).to.equal(0);
    });

    it('Can reset a command through the profiler', () => {
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(INSTANCE_NAME);
      const command = profiler.createCommand(COMMAND_NAME);

      expect(command.stats.count).to.equal(0);

      const pRun = profiler.enter(COMMAND_NAME);
      pRun();

      expect(command.stats.count).to.equal(1);
      expect(command.stats.duration.nanoSecs).to.be.above(0);
      expect(profiler.stats[COMMAND_NAME].count).to.equal(1);
      expect(profiler.command(COMMAND_NAME).stats.count).to.equal(1);

      expect(profiler.reset()).to.equal(false);
      const rv = profiler.reset(COMMAND_NAME);
      expect(rv).to.equal(true);

      expect(command.stats.count).to.equal(0);
      expect(profiler.stats[COMMAND_NAME].count).to.equal(0);
      expect(command.stats.duration.nanoSecs).to.equal(0);
    });

    it('Can auto report', async () => {
      const AUTOREPORT_INTERVAL = 10;
      const INSTANCE_NAME = 'aap';
      const COMMAND_NAME = 'noot';
      const profiler = new profilerMod.Profiler(
        INSTANCE_NAME,
        AUTOREPORT_INTERVAL
      );
      let pRun;

      pRun = profiler.enter(COMMAND_NAME);
      pRun();
      pRun = profiler.enter(COMMAND_NAME);
      pRun();

      expect(profiler.stats[COMMAND_NAME].count).to.equal(2);
      expect(profiler.stats[COMMAND_NAME].durationS).to.be.a('string');
      expect(profiler.stats[COMMAND_NAME].averageNS).to.be.a('number');

      const backupStderrWrite = process.stderr.write;
      const stdErrWriteSpy = sinon.spy();
      process.stderr.write = stdErrWriteSpy;

      // wait for autoReport
      await new Promise(resolve =>
        setTimeout(resolve, AUTOREPORT_INTERVAL * 2.5)
      );

      process.stderr.write = backupStderrWrite;
      expect(stdErrWriteSpy.callCount).to.equal(2);
    });
  });
});
